import pygame, os


class TextureGenerator(object):
    """Texture manager"""

    def __init__(self, di, ind):
        f = file(os.path.join(di, ind), 'r')
        text = f.readlines()
        self.size = int(text[0])
        self.loaded = [None] * self.size
        self.filetable = [""] * self.size
        for i in range(self.size):
            s = text[1 + i].split()
            self.filetable[int(s[0])] = os.path.join(di, s[1])


    def get_image(self, index):
        if self.loaded[index] == None:
            self.__load_image(index)

        return self.loaded[index]

    def __load_image(self, index):
        self.loaded[index] = pygame.image.load(self.filetable[index]).convert()
