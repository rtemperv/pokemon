SQUARE_WIDTH = 40
SQUARE_HEIGHT = 40

import Square


class Room(object):
    """docstring for Room"""

    def __init__(self, fle, textgnr):
        self.textgnr = textgnr
        f = file(fle, 'r').readlines()
        self.height = int(f[0])
        self.width = int(f[1])
        self.table = [None] * self.height
        self.objects = [None] * self.height
        for i in range(self.height):
            self.table[i] = [None] * self.width
            self.objects[i] = [None] * self.width
            line = map(int, f[2 + i].split())
            for j in range(self.width):
                self.table[i][j] = self.__CreateSquare(line[j], (i, j))


    """ nasty code """

    def __CreateSquare(self, index, pos):
        if index == 4:
            return Square.Grass(self.textgnr, self, pos)
        elif index == 5:
            return Square.Tree(self.textgnr, self, pos)
        elif index == 6:
            return Square.Bush(self.textgnr, self, pos)
        elif index == 7:
            return Square.Coin(self.textgnr, self, pos)

    def draw(self, surface, coorda, coordb, coordc):

        for i in range(coorda[0], coordb[0]):
            for j in range(coorda[1], coordb[1]):
                self.table[i][j].draw(surface, (coordc[0] + SQUARE_WIDTH * j, coordc[1] + SQUARE_HEIGHT * i),
                                      SQUARE_HEIGHT, SQUARE_WIDTH)
                if self.objects[i][j]:
                    self.objects[i][j].draw(surface, (coordc[0] + SQUARE_WIDTH * j, coordc[1] + SQUARE_HEIGHT * i),
                                            SQUARE_HEIGHT, SQUARE_WIDTH)

    def save():
        pass


    def isPassable(self, x, y):
        if self.height > y >= 0 and self.width > x >= 0:
            return self.table[y][x].passable
        else:
            return False

    def spawnCoin(self, pos):
        self.objects[pos[0]][pos[1]] = self.__CreateSquare(7, pos)

    def doEvents(self, pos, player):
        self.table[pos[1]][pos[0]].do_action(player)