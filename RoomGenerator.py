import pygame, os
import Room


class RoomGenerator(object):
    """RoomGenerator"""

    def __init__(self, di, ind, textgnr):
        self.textgnr = textgnr
        f = file(os.path.join(di, ind), 'r')
        text = f.readlines()
        self.size = int(text[0])
        self.loaded = [None] * self.size
        self.filetable = [""] * self.size
        for i in range(self.size):
            s = text[1 + i].split()
            self.filetable[int(s[0])] = os.path.join(di, s[1])


    def get_room(self, index):
        if index >= self.size:
            return None
        if self.loaded[index] == None:
            self.__load_room(index)
        return self.loaded[index]

    def put_room(self, room, index):
        self.loaded[index] = room
        room.save(self.filetable[index])

    def __load_room(self, index):
        self.loaded[index] = Room.Room(self.filetable[index], self.textgnr)

			