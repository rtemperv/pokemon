from TextBox import TextBox
from TextureGenerator import TextureGenerator
from RoomGenerator import RoomGenerator
from Player import Player
import pygame, time, sys


class Gameloop(object):
    """docstring for Gameloop"""

    def __init__(self):
        self.screen = pygame.display.set_mode((850, 600))
        pygame.font.init()
        self.textgnr = TextureGenerator("Textures", "index")
        self.roomgnr = RoomGenerator("Rooms", "index", self.textgnr)
        self.currentroom = self.roomgnr.get_room(0)
        self.textbox = TextBox(0,400, 600, 200, 25)
        self.player = Player((1, 1), "Bob", self.currentroom, self.textgnr, self.textbox)
        self.delay = 0.04

    def handleMovement(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self.player.move(0, -1)
                self.handleEvent()
            elif event.key == pygame.K_RIGHT:
                self.player.move(1, 0)
                self.handleEvent()
            elif event.key == pygame.K_DOWN:
                self.player.move(0, 1)
                self.handleEvent()
            elif event.key == pygame.K_LEFT:
                self.player.move(-1, 0)
                self.handleEvent()

    def handleEvent(self):

        self.currentroom.doEvents((self.player.x, self.player.y), self.player)

    def drawStuff(self):
        self.screen.fill(pygame.Color("black"))
        self.currentroom.draw(self.screen, (0, 0), (self.currentroom.height, self.currentroom.width), (0, 0))
        self.textbox.Draw(self.screen)
        self.player.draw(self.screen, (0, 0))

    def flipBuffer(self):
        pygame.display.flip()

    def handleExit(self, event):
        if event.type == pygame.QUIT:
            sys.exit(0)

    def Loop(self):
        while True:
            event = pygame.event.poll()
            self.handleMovement(event)
            self.handleExit(event)
            self.drawStuff()
            self.flipBuffer()
            time.sleep(self.delay)


def main():
    g = Gameloop()
    g.Loop()


if __name__ == '__main__':
    main()