import pygame
import Room

NORTH, EAST, SOUTH, WEST = (0, 1, 2, 3)


class Player(object):
    """docstring for Player"""

    def __init__(self, startpos, name, room, textgnr, textbox):
        self.x, self.y = startpos
        self.name = name
        self.direction = SOUTH
        self.room = room
        self.textgnr = textgnr
        self.gold = 0
        self.message = ""
        self.textbox = textbox

    def add_message(self, text):
        self.textbox.add_message(text, self)
    def move(self, hor, ver):
        if self.room.isPassable(self.x + hor, self.y + ver):
            self.x += hor
            self.y += ver
            if hor == -1:
                self.direction = WEST
            elif hor == 1:
                self.direction = EAST
            elif ver == 1:
                self.direction = SOUTH
            elif ver == -1:
                self.direction = NORTH


    def draw(self, surface, offset):
        surface.blit(self.textgnr.get_image(self.direction),
                     (offset[0] + self.x * Room.SQUARE_WIDTH, offset[1] + self.y * Room.SQUARE_HEIGHT))
        pygame.font.init()
        font = pygame.font.Font(None, 30)
        text = font.render("Name: %s" % self.name, 1, (255, 255, 255))
        textb = font.render("Gold: %d" % self.gold, 1, (255, 255, 255))
        surface.blit(text, (self.room.width * 40 + 40, 20))
        surface.blit(textb, (self.room.width * 40 + 40, 40))

        textc = font.render(self.message, 1, (255, 255, 255))
        surface.blit(textc, (40, self.room.height * 40 + 40))


		
		