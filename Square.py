import pygame
import random

SPAWNCHANCE = 0.10


class Square(object):
    """docstring for Square"""

    def __init__(self, texture, passable, textgnr, room, pos):
        self.texture = texture
        self.passable = passable
        self.textgnr = textgnr
        self.room = room
        self.x = pos[0]
        self.y = pos[1]

    def draw(self, surface, coord, height, width):
        surface.blit(pygame.transform.scale(self.textgnr.get_image(self.texture), (width, height)), coord)

    def do_action(self, player):
        pass


class Grass(Square):
    """docstring for Grass"""

    def __init__(self, textgnr, room, pos):
        super(Grass, self).__init__(4, True, textgnr, room, pos)


class Tree(Square):
    def __init__(self, textgnr, room, pos):
        super(Tree, self).__init__(5, False, textgnr, room, pos)


class Bush(Square):
    """docstring for Bush"""

    def __init__(self, textgnr, room, pos):
        super(Bush, self).__init__(6, True, textgnr, room, pos)
        goldChance = random.random()
        self.gold = 0
        if goldChance < 0.05:
            self.gold = 10
        elif goldChance < 0.15:
            self.gold = 5
        elif goldChance < 0.30:
            self.gold = 1

    def do_action(self, player):
        if self.gold != 0:
            player.add_message("You found %d gold coin(s)" % self.gold)
            player.gold += self.gold
            self.gold = 0


class Coin(Square):
    """docstring for Coin"""

    def __init__(self, textgnr, room, pos):
        super(Coin, self).__init__(7, True, textgnr, room, pos)
		
		
		
		