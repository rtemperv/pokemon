from Queue import Queue
import pygame


class TextBox():
    def __init__(self, x, y, width = 300, height = 100, font_size = 30):
        self.text_size = font_size
        self.width = width
        self.height = height
        self.x = x
        self.y = y
        self.fontr = pygame.font.Font(None, self.text_size)
        self.offset = 5
        self.max_lines = (self.height)//(self.fontr.get_height() + self.offset)
        self.render_text = self.fontr.render("This is a test", 1, (0,0,0))
        self.Rect = pygame.Rect(x, y, width, self.height)
        self.message_queue = []
    def add_message(self, text, player):
        if self.fontr.size(text)[0] > self.width + self.offset * 2:
            print  self.width
            return False
        else:
            self.message_queue.append(text)
            return True
    def clear(self):
        self.message_queue = []

    def Draw(self, canvas):
        pygame.draw.rect(canvas, (255, 255, 255), self.Rect)
        current_height = 0
        for i in self.message_queue[-self.max_lines:]:
            canvas.blit(self.fontr.render(i,1,(0,0,0)), (self.offset + self.x, current_height + self.offset + self.y ))
            current_height += self.offset + self.fontr.get_height()


if __name__ == '__main__':
    width, height = 800, 600
    clock = pygame.time.Clock()
    pygame.font.init()
    screen = pygame.display.set_mode((width, height))
    TextB = TextBox(30, 30, 400, 600)
    Run = True
    test = 0
    while Run:
        TextB.add_message("Current : %d" % test , "")
        test += 1
        screen.fill((0, 150, 250))
        TextB.Draw(screen)
        pygame.display.flip()
        clock.tick(100)